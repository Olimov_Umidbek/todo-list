package com.arion.todolist.controller;

import com.arion.todolist.dto.todo.CreateToDoDto;
import com.arion.todolist.dto.todo.ToDoDetails;
import com.arion.todolist.dto.todo.UpdateToDoDto;
import com.arion.todolist.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class ToDoController {

    @Autowired
    private ToDoService service;

    @PostMapping("/")
    public ResponseEntity<String> create(@RequestBody CreateToDoDto dto) {
        service.create(dto);
        return ResponseEntity.ok("To do created !");
    }

    @GetMapping("/{id}")
    public ResponseEntity<ToDoDetails> findOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.findOne(id));
    }

    @GetMapping("/")
    public ResponseEntity<List<ToDoDetails>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateToDo(@PathVariable Long id, @RequestBody UpdateToDoDto doDto) {
        service.update(id, doDto);
        return ResponseEntity.ok("To do updated!");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok("To do deleted!");
    }

}

