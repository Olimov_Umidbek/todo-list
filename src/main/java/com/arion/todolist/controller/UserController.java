package com.arion.todolist.controller;

import com.arion.todolist.dto.user.CreateUserDto;
import com.arion.todolist.dto.user.UpdateUserDto;
import com.arion.todolist.dto.user.UserDetails;
import com.arion.todolist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/")
    public ResponseEntity<String> create(@RequestBody CreateUserDto dto) {
        userService.create(dto);
        return ResponseEntity.ok("User created.");
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<UserDetails>> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDetails> findOne(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findOne(id));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<String> updateDetails(@PathVariable Long id, @RequestBody UpdateUserDto dto) {
        userService.update(id, dto);
        return ResponseEntity.ok("User details successfully updated");
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.ok("User deleted !");
    }

}
