package com.arion.todolist.repository;

import com.arion.todolist.entity.ToDo;
import com.arion.todolist.entity.User;
import com.arion.todolist.enums.ToDoStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDo, Long> {

    List<ToDo> findAllByUser(User user);

    List<ToDo> findAllByStatus(ToDoStatus status);
}
