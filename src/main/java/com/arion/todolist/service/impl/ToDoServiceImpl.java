package com.arion.todolist.service.impl;

import com.arion.todolist.dto.todo.CreateToDoDto;
import com.arion.todolist.dto.todo.ToDoDetails;
import com.arion.todolist.dto.todo.UpdateToDoDto;
import com.arion.todolist.entity.ToDo;
import com.arion.todolist.entity.User;
import com.arion.todolist.enums.ToDoStatus;
import com.arion.todolist.exception.NotFoundException;
import com.arion.todolist.exception.NullPointerException;
import com.arion.todolist.repository.ToDoRepository;
import com.arion.todolist.repository.UserRepository;
import com.arion.todolist.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ToDoServiceImpl implements ToDoService {

    @Autowired
    private ToDoRepository toDoRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void create(CreateToDoDto toDo) {
        if (toDo == null) {
            throw new NullPointerException(100, "Your data is null");
        }

        toDoRepository.save(
                fetchEntity(toDo));
    }

    @Override
    public void update(Long id, UpdateToDoDto updateToDoDto) {
        if (id == null) {
            throw new NullPointerException(100, "Your id is null");
        }

        Optional<ToDo> optionalToDo = toDoRepository.findById(id);

        if (!optionalToDo.isPresent()) {
            throw new NotFoundException(200, "This todo is not found, please check your id");
        }

        toDoRepository.save(updateToDoDetails(updateToDoDto, optionalToDo.get()));
    }

    @Override
    public void delete(Long id) {
        if (id == null) {
            throw new NullPointerException(100, "Your id is null");
        }

        Optional<ToDo> optionalToDo = toDoRepository.findById(id);

        if (!optionalToDo.isPresent()) {
            throw new NotFoundException(200, "This todo is not found, please check your id");
        }

        toDoRepository.delete(optionalToDo.get());
    }

    @Override
    public ToDoDetails findOne(Long id) {
        if (id == null) {
            throw new NullPointerException(100, "Your id is null");
        }

        Optional<ToDo> optionalToDo = toDoRepository.findById(id);

        if (!optionalToDo.isPresent()) {
            throw new NotFoundException(200, "This todo is not found, please check your id");
        }

        return fetchToDoDetails(optionalToDo.get());
    }

    @Override
    public List<ToDoDetails> findAll() {
        List<ToDo> toDoList = toDoRepository.findAll();
        List<ToDoDetails> detailsList = new ArrayList<>();

        toDoList.parallelStream().forEach(toDo -> detailsList.add(fetchToDoDetails(toDo)));

        return detailsList;
    }

    @Override
    public List<ToDoDetails> findByUser(Long userId) {
        if (userId == null) {
            throw new NullPointerException(100, "Check your id");
        }

        List<ToDo> toDoList = toDoRepository.findAllByUser(fetchUserEntity(userId));
        List<ToDoDetails> detailsList = new ArrayList<>();

        toDoList.parallelStream().forEach(toDo -> detailsList.add(fetchToDoDetails(toDo)));

        return detailsList;
    }

    @Override
    public List<ToDoDetails> findByStatus(ToDoStatus status) {
        if (status == null) {
            throw new NullPointerException(100, "Check your session");
        }

        List<ToDo> toDoList = toDoRepository.findAllByStatus(status);
        List<ToDoDetails> detailsList = new ArrayList<>();

        toDoList.parallelStream().forEach(toDo -> detailsList.add(fetchToDoDetails(toDo)));

        return detailsList;
    }

    private ToDo fetchEntity(CreateToDoDto createToDoDto) {
        ToDo toDo = new ToDo();
        if (!createToDoDto.getName().isEmpty()) {
            toDo.setName(createToDoDto.getName());
        }

        if (createToDoDto.getStatus() != null) {
            toDo.setStatus(createToDoDto.getStatus());
        }
        toDo.setUserId(createToDoDto.getUserId());
        toDo.setUser(fetchUserEntity(createToDoDto.getUserId()));
        toDo.setCreatedDate(new Date());

        return toDo;
    }

    private ToDo updateToDoDetails(UpdateToDoDto source, ToDo target) {
        if (!source.getName().isEmpty()) {
            target.setName(source.getName());
        }

        if (source.getStatus() != null) {
            target.setStatus(source.getStatus());
        }

        target.setUpdatedDate(new Date());

        return target;
    }

    private ToDoDetails fetchToDoDetails(ToDo toDo) {
        ToDoDetails details = new ToDoDetails();

        details.setId(toDo.getId());
        details.setName(toDo.getName());
        details.setStatus(toDo.getStatus());
        details.setCreatedDate(toDo.getCreatedDate());
        details.setUpdatedDate(toDo.getUpdatedDate());
        details.setUserId(toDo.getUser().getId());

        return details;
    }

    private User fetchUserEntity(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);

        if (!optionalUser.isPresent()) {
            throw new NotFoundException(200, "User not found, please check your user id");
        }

        return optionalUser.get();
    }

}
