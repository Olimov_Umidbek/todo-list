package com.arion.todolist.service.impl;

import com.arion.todolist.dto.todo.ToDoDetails;
import com.arion.todolist.dto.user.CreateUserDto;
import com.arion.todolist.dto.user.UpdateUserDto;
import com.arion.todolist.dto.user.UserDetails;
import com.arion.todolist.entity.ToDo;
import com.arion.todolist.entity.User;
import com.arion.todolist.exception.NotFoundException;
import com.arion.todolist.exception.NullPointerException;
import com.arion.todolist.repository.UserRepository;
import com.arion.todolist.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void create(CreateUserDto createUserDto) {
        if (createUserDto == null) {
            throw new NullPointerException(100, "User data is equal to null");
        }
        userRepository.save(fetchUserEntity(createUserDto));
    }

    @Override
    public void update(Long id, UpdateUserDto updateUserDto) {
        if (id == null) {
            throw new NullPointerException(100, "This id is null");
        }
        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent()) {
            throw new NotFoundException(200, "This user not found, please check your id");
        }
        User user = optionalUser.get();
        user.setName(updateUserDto.getName());
        user.setMiddleName(updateUserDto.getMiddleName());
        user.setSurname(updateUserDto.getSurname());
        user.setAge(updateUserDto.getAge());
        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        if (id == null) {
            throw new NullPointerException(100, "This id is null");
        }
        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent()) {
            throw new NotFoundException(200, "This use not found, please check your id");
        }
        userRepository.delete(optionalUser.get());
    }

    @Override
    public UserDetails findOne(Long id) {
        if (id == null) {
            throw new NullPointerException(100, "This id is null");
        }

        Optional<User> optionalUser = userRepository.findById(id);

        if (!optionalUser.isPresent()) {
            throw new NotFoundException(200, "This  use not found, please check your id");
        }

        return fetchUserDetails(optionalUser.get());
    }

    @Override
    public List<UserDetails> findAll() {
        List<User> userList = userRepository.findAll();
        List<UserDetails> userDetailsList = new ArrayList<>();

        userList.parallelStream().forEach(user -> userDetailsList.add(fetchUserDetails(user)));
        return userDetailsList;
    }

    private User fetchUserEntity(@NotNull CreateUserDto dto) {
        User user = new User();

        if (!dto.getName().isEmpty()) {
            user.setName(dto.getName());
        }

        if (!dto.getSurname().isEmpty()) {
            user.setSurname(dto.getSurname());
        }

        if (!dto.getMiddleName().isEmpty()) {
            user.setMiddleName(dto.getMiddleName());
        }

        if (dto.getAge() != null) {
            user.setAge(dto.getAge());
        }

        return user;
    }

    private UserDetails fetchUserDetails(User user) {
        return new UserDetails(
                user.getId(),
                user.getName(),
                user.getSurname(),
                user.getMiddleName(),
                user.getAge(),
                fetchToDoDetails(user.getToDoList()));
    }

    private List<ToDoDetails> fetchToDoDetails(List<ToDo> toDoList) {
        List<ToDoDetails> detailsList = new ArrayList<>();
        toDoList.parallelStream().forEach(toDo -> {

            ToDoDetails details = new ToDoDetails();

            details.setId(toDo.getId());
            details.setName(toDo.getName());
            details.setStatus(toDo.getStatus());
            details.setUserId(toDo.getUser().getId());
            details.setCreatedDate(toDo.getCreatedDate());
            details.setUpdatedDate(toDo.getUpdatedDate());

            detailsList.add(details);
        });
        return detailsList;
    }
}
