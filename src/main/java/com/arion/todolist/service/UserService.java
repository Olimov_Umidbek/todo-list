package com.arion.todolist.service;

import com.arion.todolist.dto.user.CreateUserDto;
import com.arion.todolist.dto.user.UpdateUserDto;
import com.arion.todolist.dto.user.UserDetails;

import java.util.List;

public interface UserService {

    void create(CreateUserDto user);

    void update(Long id, UpdateUserDto user);

    void delete(Long id);

    UserDetails findOne(Long id);

    List<UserDetails> findAll();
}
