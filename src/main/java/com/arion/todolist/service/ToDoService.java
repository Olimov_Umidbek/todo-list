package com.arion.todolist.service;

import com.arion.todolist.dto.todo.CreateToDoDto;
import com.arion.todolist.dto.todo.ToDoDetails;
import com.arion.todolist.dto.todo.UpdateToDoDto;
import com.arion.todolist.enums.ToDoStatus;

import java.util.List;

public interface ToDoService {

    void create(CreateToDoDto toDo);

    void update(Long id, UpdateToDoDto toDo);

    void delete(Long id);

    ToDoDetails findOne(Long id);

    List<ToDoDetails> findAll();

    List<ToDoDetails> findByUser(Long userId);

    List<ToDoDetails> findByStatus(ToDoStatus status);
}
