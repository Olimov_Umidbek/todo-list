package com.arion.todolist.dto.user;

import com.arion.todolist.dto.todo.ToDoDetails;

import java.util.List;

public class UserDetails {

    private Long id;
    private String name;
    private String surname;
    private String middleName;
    private Long age;
    private List<ToDoDetails> toDoDetailsList;

    public UserDetails() {
    }

    public UserDetails(Long id, String name, String surname, String middleName, Long age, List<ToDoDetails> toDoDetailsList) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.age = age;
        this.toDoDetailsList = toDoDetailsList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public List<ToDoDetails> getToDoDetailsList() {
        return toDoDetailsList;
    }

    public void setToDoDetailsList(List<ToDoDetails> toDoDetailsList) {
        this.toDoDetailsList = toDoDetailsList;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", age=" + age +
                ", toDoDetailsList=" + toDoDetailsList +
                '}';
    }
}
