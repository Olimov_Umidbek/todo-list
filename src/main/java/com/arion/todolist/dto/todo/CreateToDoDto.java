package com.arion.todolist.dto.todo;

import com.arion.todolist.enums.ToDoStatus;

public class CreateToDoDto {

    private String name;
    private ToDoStatus status;
    private Long userId;

    public CreateToDoDto() {

    }

    public CreateToDoDto(String name, ToDoStatus status, Long userId) {
        this.name = name;
        this.status = status;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ToDoStatus getStatus() {
        return status;
    }

    public void setStatus(ToDoStatus status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "CreateToDoDto{" +
                "name='" + name + '\'' +
                ", status=" + status +
                ", userId=" + userId +
                '}';
    }
}
