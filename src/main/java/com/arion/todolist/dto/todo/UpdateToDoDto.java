package com.arion.todolist.dto.todo;

import com.arion.todolist.enums.ToDoStatus;

public class UpdateToDoDto {

    private String name;
    private ToDoStatus status;

    public UpdateToDoDto() {

    }

    public UpdateToDoDto(String name, ToDoStatus status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ToDoStatus getStatus() {
        return status;
    }

    public void setStatus(ToDoStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UpdateToDoDto{" +
                "name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
