package com.arion.todolist.dto.todo;

import com.arion.todolist.enums.ToDoStatus;

import java.util.Date;

public class ToDoDetails {

    private Long id;
    private String name;
    private ToDoStatus status;
    private Date createdDate;
    private Date updatedDate;
    private Long userId;

    public ToDoDetails() {

    }

    public ToDoDetails(Long id, String name, ToDoStatus status, Date createdDate, Date updatedDate, Long userId) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ToDoStatus getStatus() {
        return status;
    }

    public void setStatus(ToDoStatus status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ToDoDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                ", userId=" + userId +
                '}';
    }
}
