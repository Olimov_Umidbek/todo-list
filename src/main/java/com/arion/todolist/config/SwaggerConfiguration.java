package com.arion.todolist.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private Predicate<String> paths;

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.arion.todolist.controller"))
                .paths(getPaths())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(true);
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "To do list API documentation",
                "Documentation for to do list api",
                "v1.0",
                "Term of service",
                new Contact("Umidbek Olimov", "https://bitbucket.org/Olimov_Umidbek", "umidbek.olimov.97@gmail.com"),
                "License of API",
                "API license URL");
    }

    private Predicate<String> getPaths() {
        return Predicates
                .and(PathSelectors.regex("/.*"),
                        Predicates
                                .not(PathSelectors.regex("/error.*")));
    }

}
