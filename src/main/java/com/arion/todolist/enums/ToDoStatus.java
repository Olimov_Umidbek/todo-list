package com.arion.todolist.enums;

public enum ToDoStatus {
    NEW,
    FINISHED,
    CANCELLED;

    public String authority() {
        return this.name();
    }

}
